// fold.go

package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

func fold(w io.Writer, r io.Reader, width int) error {
	scanner := bufio.NewScanner(r)
	for scanner.Scan() {
		line := scanner.Text()
		array := strings.Split(line, "")
		for {
			min := len(array)
			if width < min {
				min = width
			}
			slice := array[:min]
			array = array[min:]
			fmt.Fprintln(w, strings.Join(slice, ""))
			if 0 == len(array) {
				break
			}
		}
	}
	return scanner.Err()
}

func main() {
	const (
		widthdefault = 15
		widthhelp = "max width to fold at"
	)
	var width int
	flag.IntVar(&width, "w", widthdefault, widthhelp+" (shorthand)")
	flag.IntVar(&width, "width", widthdefault, widthhelp)
	flag.Parse()
	out := os.Stdout
	if 0 == flag.NArg() {
		if err := fold(out, os.Stdin, width); nil != err {
			log.Fatal(err)
		}
	} else {
		for i := 0; i < flag.NArg(); i++ {
			if "-" == flag.Arg(i) {
				if err := fold(out, os.Stdin, width); nil != err {
					log.Fatal(err)
				}
				continue
			}
			var file *os.File
			var err error
			if file, err = os.Open(flag.Arg(i)); nil != err {
				log.Fatal(err)
			}
			if err = fold(out, file, width); nil != err {
				file.Close()
				log.Fatal(err)
			}
			file.Close()
		}
	}
}
