#!/bin/python
# On Python2:
# pip install future

from __future__ import unicode_literals, print_function
from builtins import bytes, str
import argparse
import fileinput
import sys

parser = argparse.ArgumentParser(description='Fold text lines')
parser.add_argument('files', nargs='*', metavar='FILE',
    help='FILE with text to fold (default: stdin)')
parser.add_argument('-w', '--width', type=int, default=15,
    help='column to fold the text lines at')
args = parser.parse_args()
try:
    for line in fileinput.input(args.files):
        line = bytes(line, encoding='utf-8')
        line = str(line, encoding='utf-8')
        line = line.rstrip('\n')
        while True:
            slice = line[:args.width]
            line = line[args.width:]
            print(slice)
            if 0 == len(line):
                break
except Exception as e:
    print(e, file=sys.stderr)
    sys.exit(1)
